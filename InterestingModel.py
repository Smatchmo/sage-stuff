H_list = []
for i in range (0, len(C_list)):
	C = C_list[i]
	D = distance(O,C)
	Cx = int(C[1])
	Cy = int(C[2])
	if Cx < 0 and Cy < 0:
		H = (0, Cx-(D/2), Cy-(D/2))
		H2O += Sphere(.05, color='pink').translate(H)
	if Cx > 0 and Cy > 0:
		H = (0, Cx+(D/2), Cy+(D/2))
		H2O += Sphere(.05, color='pink').translate(H)
	if Cx == 0 and Cy > 0:
		H = (0, 0, Cy+(D/2))
		H2O += Sphere(.05, color='pink').translate(H)
	if Cx == 0 and Cy < 0:
		H = (0, 0, Cy-(D/2))
		H2O += Sphere(.05, color='pink').translate(H)
