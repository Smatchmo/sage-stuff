#/user/rgmac2/SageMolClass.py


 L = [[cos(pi*i/3),sin(pi*i/3)] for i in range(6)]
 
from sage.plot.plot3d.shapes import LineSegment, Sphere
from sage.plot.colors import rainbow
from sage.plot.plot3d.implicit_plot3d import implicit_plot3d
from sage.plot.plot3d.shapes2 import text3d
from sage.repl.rich_output.pretty_print import show
from sage.calculus.var import var
from sage.plot.circle import circle
from sage.functions.trig import cos

Radii = {'O': 0.73, 'N': 0.75, 'C': 0.77, 'He': 0.32, 'H': 0.37, 'S': 1.02, 'Cl': 0.99,

    'F': 0.71, 'Xe': 1.30, 'Si': 1.11, 'B': 0.82, 'P': 1.06, 'Br': 1.14}


Colors = {'O': 'red', 'N': 'darkblue', 'C': 'black', 'He': 'cyan', 'H': 'white', 'S': 'yellow',

	'Si': 'khaki', 'Xe': 'lightskyblue', 'F': 'green', 'Cl': 'limegreen', 'B': 'lightgoldenrodyellow', 'P': 'orange',

	'Br': 'darkred'}

Mol = Sphere(.00001, color = 'white').translate((0,0,0))

col = rainbow(10)

#--------------------------------------------------------------------------------------------------------------------------------

class Benzene_Ring(object):

	def pi_ring_method(self,Ax,Ay):

		delta = 0.05

		if Ax[1] == Ay[1]:
			
			Ax1 = (Ax[0], Ax[1]-delta, Ax[2])
			Ax2 = (Ax[0], Ax[1]+delta, Ax[2])
			Ay1 = (Ay[0], Ay[1]-delta, Ay[2])
			Ay2 = (Ay[0], Ay[1]+delta, Ay[2])

			PiBond = LineSegment(Ax1, Ay1, 1, color = 'white')
			PiBond += LineSegment(Ax2, Ay2, 1, color = 'white')

			return (PiBond)

		else:

			Ax1 = (Ax[0], Ax[1], Ax[2]-delta)
			Ax2 = (Ax[0], Ax[1], Ax[2]+delta)
			Ay1 = (Ay[0], Ay[1], Ay[2]-delta)
			Ay2 = (Ay[0], Ay[1], Ay[2]+delta)

			PiBond = LineSegment(Ax1, Ay1, 1, color = 'white')
			PiBond += LineSegment(Ax2, Ay2, 1, color = 'white')

			return (PiBond)

#---------------------------------------------------------------------------------------------------------------------------

class Planar(object):

	def pi_planar_method(self,Ax,Ay):

		if (Ax[0] == Ay[0]) and (Ax[1] == Ay[1]):

			delta = 0.05
			
			Ax1 = (Ax[0], Ax[1]-delta, Ax[2])
			Ax2 = (Ax[0], Ax[1]+delta, Ax[2])
			Ay1 = (Ay[0], Ay[1]-delta, Ay[2])
			Ay2 = (Ay[0], Ay[1]+delta, Ay[2])

			PiBond = LineSegment(Ax1, Ay1, 1, color = 'white')
			PiBond += LineSegment(Ax2, Ay2, 1, color = 'white')

			return (PiBond)

		if Ax[1] == Ay[1] and Ax[2] == Ay[2]:

			delta = 0.05

			Ax1 = (Ax[0], Ax[1], Ax[1]-delta)
			Ax2 = (Ax[0], Ax[1], Ax[1]+delta)
			Ay1 = (Ay[0], Ay[1], Ay[1]-delta)
			Ay2 = (Ay[0], Ay[1], Ay[1]+delta)

			PiBond = LineSegment(Ax1, Ay1, 1, color = 'white')
			PiBond += LineSegment(Ax2, Ay2, 1, color = 'white')

			return (PiBond)

		if Ay[1] == 0.75:

			delta = 0.05

			Ax1 = (Ax[0], Ax[1], Ax[1]-delta)
			Ax2 = (Ax[0], Ax[1], Ax[1]+delta)
			Ay1 = (Ay[0], Ay[1], -Ay[1]-delta)
			Ay2 = (Ay[0], Ay[1], -Ay[1]+delta)

			PiBond = LineSegment(Ax1, Ay1, 1, color = 'white')
			PiBond += LineSegment(Ax2, Ay2, 1, color = 'white')

			return (PiBond)

		if Ay[1] == -0.75:

			delta = 0.05

			Ax1 = (Ax[0], Ax[1]-delta, Ax[2])
			Ax2 = (Ax[0], Ax[1]+delta, Ax[2])
			Ay1 = (Ay[0], Ay[1]-delta, Ay[2])
			Ay2 = (Ay[0], Ay[1]+delta, Ay[2])

			PiBond = LineSegment(Ax1, Ay1, 1, color = 'white')
			PiBond += LineSegment(Ax2, Ay2, 1, color = 'white')

			return (PiBond)
#---------------------------------------------------------------------------------------------------------------------------

def Normalize(x):

	x = (x/0.77)*0.35

	return x

#-------------------------------------------------------------------------------------------------------------------------

#def Benzene(Mol):

#	Benzene = Benzene_Ring()

#	A = {1: (0, 0, 1.118), 2: (0, 1, 0.559), 3: (0, 1, -0.559), 4: (0, 0,-1.118), 5: (0, -1, -0.559), 6: (0, -1, 0.559) }

#	Pi_Dict = { 'A1-A2' : [A[1],A[2]], 'A5-A6' : [A[5],A[6]], 'A3-A4': [A[3],A[4]] }

#	for key, value in Pi_Dict.items():	

#		Mol += Benzene.pi_ring_method(*value)

#	show(Mol)

#------------------------------------------------------------------------------------------------------------------------------

def bent_geometry(XY2,Pi_Key,Pi_Bond):

	A = { 0 : (0,0,0) }

	S = { 2 : (0, 0.75, -0.75), 1 : (0, -0.75,-0.75) }

	C2R = { 1 : (0,0,1.25), 2 : (0,0,-2) } #label these better so as to represent the corresponding atoms?

	C2R_pair = { 1 : [C2R[1], C2R[2]] }

	C2R_label = { (0,0,1.5) : 'C2' }

	Central_Atom = { A[0] : 'O' }

	Central_Atom_Swap = { A[0]: '' }

	Substituents = { S[1] : 'H', S[2] : 'H' }

	Sub_Swap = { S[1] : '', S[2] : '' }

	Sigma_Dict = { 1 : [ A[0],S[1] ], 2 : [ A[0],S[2] ] }

	Pi_Dict = { 1 : [ A[0],S[1] ], 2 : [ A[0],S[2] ] }

	Sym_Dict = { 'yz' : [(-1,1),(-1.25,1.25),(-1.5,0.75)], 'xz' : [(-1,1),(-1.25,1.25),(-1.5,0.75)] }

	Bent_Atoms = { }

	bent_mol = Planar() #add argument?

	if (Central_Atom_Swap[A[0]] != ''):

		Central_Atom.clear()

	if (Central_Atom_Swap[A[0]] == ''):

		Central_Atom_Swap.clear()

	for sub in Sub_Swap:

		if (Sub_Swap[sub] != '') and (sub in Substituents):

			del Substituents[sub]

	for i in list(Sub_Swap):

		if (Sub_Swap[i] == ''):

			del Sub_Swap[i]

	if Pi_Key == 'pi' and Pi_Bond == 'both':

		Sigma_Dict.clear()

		for key, value in Pi_Dict.items():

			XY2 += bent_mol.pi_planar_method(*value)

	if (Pi_Key == 'pi' and Pi_Bond == 1) or (Pi_Key == 'pi' and Pi_Bond ==2):

		del Sigma_Dict[Pi_Bond]

		XY2 += bent_mol.pi_planar_method(*Pi_Dict[Pi_Bond])

	Central_Atom.update(Central_Atom_Swap)

	Substituents.update(Sub_Swap)

	if Substituents[S[1]] != Substituents[S[2]]:

		del (Sym_Dict['xz'], C2R_pair[1], C2R_label[(0,0,1.5)])

	Bent_Atoms = {**Substituents, **Central_Atom}
	
	for atom in Bent_Atoms:

		XY2 += Sphere(Normalize(Radii[Bent_Atoms[atom]]), color=Colors[Bent_Atoms[atom]]).translate(atom)

	for key, value in Sigma_Dict.items():

		XY2 += LineSegment(*value,1, color = 'white')

	for key, value in C2R_pair.items():

		XY2 += LineSegment(*value,0.5, color = col.pop())

	for label in C2R_label:

		XY2 += text3d(C2R_label[label],label, color=col.pop())

	#for plane in Sym_Dict:

	#	XY2 += Planes(XY2,Sym_Dict[plane],plane)

	return XY2
#-------------------------------------------------------------------------------------------------------------------------

show(bent_geometry(Mol,'pi','both'))
