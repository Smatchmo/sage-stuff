import os
import sys
from sage.plot.plot3d.shapes import LineSegment, Sphere
from sage.plot.colors import rainbow
from sage.plot.plot3d.implicit_plot3d import implicit_plot3d
from sage.plot.plot3d.shapes2 import text3d
from sage.repl.rich_output.pretty_print import show
from sage.calculus.var import var
from sage.functions.trig import sin, cos

pi = 3.14159
X = 0

x = var('x')
y = var('y')
z = var('z')

L = []
for i in range(6):
	Lx = (0,2*cos(pi*i/3),2*sin(pi*i/3)) 
	L.append(Lx)

A = {1: L[0], 2: L[1], 3: L[2], 4: L[3], 5: L[4], 6: L[5] }

S = {1: (1.75*X, 0, 1.75*1.118), 2: (1.75*X, 1.75*1, 1.75*0.559), 3: (1.75*X, 1.75*1, 1.75*-0.559), 4: (1.75*X, 0, 1.75*-1.118),
	5: (1.75*X, 1.75*-1, 1.75*-0.559), 6: (1.75*X, 1.75*-1, 1.75*0.559)}

C2R = {1: (X, 0, 2), 2: (1.75*X, 1.25*1.75*1, 1.25*1.75*0.559), 3: (1.75*X, 1.25*1.75*1, 1.25*1.75*-0.559), 4: (X, 0, -2),
	5: (1.75*X, 1.25*1.75*-1, 1.25*1.75*-0.559), 6: (1.75*X, 1.25*1.75*-1, 1.25*1.75*0.559) }

LC2 = { 1 : (X, 0, 2*1.1), 2 : (1.75*X, 1.1*1.25*1.75*1, 1.1*1.25*1.75*0.559), 3 : (1.75*X, 1.1*1.25*1.75*1, 1.1*1.25*1.75*-0.559), 
	4 : (X, 0, 1.1*-2), 5 : (1.75*X, 1.1*1.25*1.75*-1, 1.1*1.25*1.75*-0.559), 6 : (1.75*X, 1.1*1.25*1.75*-1, 1.1*1.25*1.75*0.559) }

LC3 = {1: (X+2.25,0,0)}

C3R = {1: (X-2,0,0), 2: (X+2,0,0)}

O = (X,0,0)

col = rainbow(10)

Benzene = Sphere(.000001, color = 'white').translate(O)

Radii = { 'O' : 0.73, 'N' : 0.75, 'C' : 0.77, 'He' : 0.32, 'H' : 0.37, 'S' : 1.02, 'Cl' : 0.99, 
    'F' : 0.71, 'Xe' : 1.30, 'Si' : 1.11, 'B' : 0.82, 'P' : 1.06, 'Br' : 1.14 }

Valency = { 'O' : 6, 'N' : 5, 'C' : 4, 'He' : 0, 'H' : 1, 'S' : 6, 'Cl' : 7, 'F' : 7, 'Xe' : 8, 'Si' : 4, 'B' : 3, 'P' : 5, 'Br' : 7}

Colors = { 'O' : 'red', 'N' : 'green', 'C' : 'black', 'He' : 'cyan', 'H' : 'white', 'S' : 'yellow',
	'Si' : 'purple', 'Xe' : 'pink', 'F' : 'blue', 'Cl' : 'green', 'B' : 'pink', 'P' : 'orange',
	'Br' : 'red' }

Sigma_Dict = { 'A1-A2' : [A[1],A[2]], 'A2-A3' : [A[2],A[3]], 'A3-A4' : [A[3],A[4]],
'A4-A5' : [A[4],A[5]], 'A5-A6' : [A[5],A[6]], 'A1-A6' : [A[1],A[6]] }

Pi_Dict = { 'A1-A2' : [A[1],A[2]], 'A5-A6' : [A[5],A[6]], 'A3-A4': [A[3],A[4]] }

Atomic_Position = { A[1]: 'C', A[2]: 'C', A[3]: 'C', A[4]: 'C', A[5]: 'C', A[6]: 'C' }

Sub_Position = { S[1]: 'H', S[2]: 'H', S[3]: 'H', S[4]: 'H', S[5]: 'H', S[6]: 'H' }

Sub_Bonds = { 'A1-S1' : [A[1],S[1]], 'A2-S2' : [A[2],S[2]], 'A3-S3' : [A[3],S[3]], 'A4-S4' : [A[4],S[4]], 'A5-S5' : [A[5],S[5]],
	 'A6-S6' : [A[6],S[6]] }

#Create common ring substituent dictionary ie OH, NO2 etc and add them to a dictionary

Subs = { S[2] : 'O', S[4]: 'H', S[6]: 'H'}

#for i in range (0,len(C2R)):

Axes_labels = { LC2[1]: 'C2', LC2[2]: 'C2', LC2[3]: 'C2', LC2[4]: 'C2', LC2[5]: 'C2', LC2[6]: 'C2' , LC3[1]: 'C3'}

C2_Rotational_Pairs = { 'C2_1': [C2R[1],C2R[4]], 'C2_2': [C2R[2],C2R[5]], 'C2_3': [C2R[3], C2R[6]] }

C3_Rotational_Pairs = { 'C6': [C3R[1], C3R[2]] }

Symmetric_Planes = {x: 'yz', y: 'xz', z: 'xy'}

def Normalize(x):
	x = (float(x)/0.77)*0.20
	return x

def bond_split(AxAy):
	x = AxAy[0]
	y = AxAy[1]
	return(x,y)

def Substituents(X,x):

	for substituent in Sub_Position:
		x += Sphere(Normalize(Radii[Sub_Position[substituent]]), color=Colors[Sub_Position[substituent]]).translate(substituent)
	for key, value in Sub_Bonds.items():		
		x += LineSegment(*value,1, color = 'white')

	x += Sym(X,x)

	return x

def Sym(X,x):

	x += Rotational_axes(X,x)
	x += Label_Baby_Jr(X,x)
	x += Planes(X,x)

	return x

def Planes(X,x):

	yz = implicit_plot3d(lambda x,y,z: x, (-2,2), (-2,2), (-2,2),color='white',opacity=0.8)
	xz = implicit_plot3d(lambda x,y,z: y, (-2,2), (-2,2), (-2,2),color='cyan',opacity=0.8)
	xy = implicit_plot3d(lambda x,y,z: z, (-2,2), (-2,2), (-2,2),color='grey',opacity=0.8)

	Planar = {'yz': yz, 'xz': xz, 'xy': xy}

	col = ['white', 'cyan', 'aquamarine']
	#for plane in Symmetric_Planes:
	x += implicit_plot3d(lambda x,y,z: x, (-2,2), (-2,2), (-2,2),color='white',opacity=0.8)
	x += implicit_plot3d(lambda x,y,z: y, (-2,2), (-2,2), (-2,2),color='cyan',opacity=0.8)
	#x += implicit_plot3d(lambda x,y,z: z, (-2,2), (-2,2), (-2,2),color=col.pop(),opacity=0.8)
	return x

def Label_Baby_Jr(X,x):

	for label in Axes_labels:
		x += text3d(Axes_labels[label],label)
	return x

def Rotational_axes(X,x):
	col = rainbow(8)
	for key, value in C2_Rotational_Pairs.items():		
		x += LineSegment(*value,0.5, color = 'blue')
	for key, value in C3_Rotational_Pairs.items():		
		x += LineSegment(*value,0.5, color = 'red')
	return x

def Pi(Ax,Ay):
	delta = 0.05
	if Ax[1] == Ay[1]:
		Aa = Ax[1]-delta
		Ab = Ax[1]+delta
		Ac = Ay[1]-delta
		Ad = Ay[1]+delta
		Ax1 = (Ax[0], Aa, Ax[2])
		Ax2 = (Ax[0], Ab, Ax[2])
		Ay1 = (Ay[0], Ac, Ay[2])
		Ay2 = (Ay[0], Ad, Ay[2])
		PiBond = LineSegment(Ax1, Ay1, 1, color = 'white')
		PiBond += LineSegment(Ax2, Ay2, 1, color = 'white')
		return (PiBond)
	else:
		Aa = Ax[2]-delta
		Ab = Ax[2]+delta
		Ac = Ay[2]-delta
		Ad = Ay[2]+delta
		Ax1 = (Ax[0], Ax[1], Aa)
		Ax2 = (Ax[0], Ax[1], Ab)
		Ay1 = (Ay[0], Ay[1], Ac)
		Ay2 = (Ay[0], Ay[1], Ad)
		PiBond = LineSegment(Ax1, Ay1, 1, color = 'white')
		PiBond += LineSegment(Ax2, Ay2, 1, color = 'white')
		return (PiBond)

for atom in Atomic_Position:
	Benzene += Sphere(Normalize(Radii[Atomic_Position[atom]]), color=Colors[Atomic_Position[atom]]).translate(atom)

for pi in Pi_Dict:
	if pi in Sigma_Dict:
		del Sigma_Dict[pi]

for key, value in Sigma_Dict.items():		
		Benzene += LineSegment(*value,1, color = 'white')

for key, value in Pi_Dict.items():		
		Benzene += Pi(*value)

for sub in Subs:
	if sub in Sub_Position:
		del Sub_Position[sub]

#Sigma_Dict.update(Pi_Dict)

Sub_Position.update(Subs)

show(Substituents(X,Benzene),frame=False)


show(Sym(X,Benzene),frame=False)

#Create a dictioanry for bond of fusion for fusing rings? IE for which bonds to join two rings together
#All coordinates
# Ring1 = {C1-C2, C2-C3, C2-C3, C4-C5, C5-C6}
# Ring2 = {R1-R2, R3-R4 etc..}
# Shared_Bond = {A2-A3}
#???
#create a function for remaining points of ring based on bond pass shared points into a function and get out remaining points?









