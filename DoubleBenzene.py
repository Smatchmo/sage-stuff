
import sys
from sage.plot.plot3d.shapes import LineSegment, Sphere
from sage.plot.colors import rainbow
from sage.plot.plot3d.implicit_plot3d import implicit_plot3d
from sage.plot.plot3d.shapes2 import text3d
from sage.repl.rich_output.pretty_print import show

BC = ['white','black','blue','red','grey','red','pink','cyan']

Radii = {'O': 0.73, 'N': 0.75, 'C': 0.77, 'He': 0.32, 'H': 0.37, 'S': 1.02, 'Cl': 0.99, 
    'F': 0.71, 'Xe': 1.30, 'Si': 1.11, 'B': 0.82, 'P': 1.06, 'Br': 1.14}

Valency = {'O': 6, 'N': 5, 'C': 4, 'He': 0, 'H': 1, 'S': 6, 'Cl': 7, 
    'F': 7, 'Xe': 8, 'Si': 4, 'B': 3, 'P': 5, 'Br': 7}

Colors = {'O': 'red', 'N': 'green', 'C': 'black', 'He': 'cyan', 'H': 'white', 'S': 'yellow',
	'Si': 'purple', 'Xe': 'pink', 'F': 'blue', 'Cl': 'green', 'B': 'pink', 'P': 'orange',
	'Br': 'red'}


def constituent_placement(x,X,Y,Z):
	O = (X,0,0)
	A2 = (X, 1, 0.5)
	A3 = (X, 1, -0.5)
	A5 = (X, -1, -0.5)
	A6 = (X, -1, 0.5)
	A1 = (X, 0, distance(A2,A3))
	A4 = (X, 0,-1*distance(A2,A3))
	Constituents = [A1,A2,A3,A4,A5,A6]
	Position = []
	Rad_list = []
	Norm_Rads = []
	Col_list = []
	for order in Order:
		if int(order) == 1:
			Position.append(A1)
		elif int(order) == 2:
			Position.append(A2)
		if int(order) == 3:
			Position.append(A3)
		elif int(order) == 4:
			Position.append(A4)
		if int(order) == 5:
			Position.append(A5)
		elif int(order) == 6:
			Position.append(A6)
	for atom in Atoms:
		for radius in Radii:
			if atom == str(radius):
				Rad_list.append(Radii[atom])
	for atom in Atoms:
		for color in Colors:
			if atom == str(color):
				Col_list.append(Colors[atom])
	for i in range (0,len(Rad_list)):
		x += Sphere(Normalize(Rad_list[i]), color=Col_list[i]).translate(Position[i])

	for i in Constituents[:]:
		if i in Position:
			Constituents.remove(i)


def atomic_class():

	A = input('Enter press enter for simple Benzene model, otherwise type "1": ')
	while (A != ""):
		B = input('Enter non carbon ring constituents in "X,Y,Z" format: ')
		C = input('Enter non carbon positions in "1,2,3" format: ')


    if (T_r == ""):
        break

		B = input('Enter non carbon ring constituents in "X,Y,Z" format: ')
		C = input('Enter non carbon positions in "1,2,3" format: ')
		NonCarbon1 = B.strip()
		NonCarbon = NonCarbon1.split(',')
		NonCarbonOrder = C.strip()
		Order = NonCarbonOrder.split(',')
		Atoms = dict(zip(NonCarbon,Order))
		return (Atoms,Order)



