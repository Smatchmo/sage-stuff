import sys
from sage.plot.plot3d.shapes import LineSegment, Sphere
from sage.plot.colors import rainbow
from sage.plot.plot3d.implicit_plot3d import implicit_plot3d
from sage.plot.plot3d.shapes2 import text3d
from sage.repl.rich_output.pretty_print import show

#---------------------------------------------------

L = []

for i in range(6):
	Lx = (0,1.5*cos(pi*i/3),1.5*sin(pi*i/3)) 
	L.append(Lx)

#----------------------------------------------------

Occupancy = []
Master_Coordinates = []
pi_list = []
pi_Check = []
H_list = []

def X_Shift(x):
	return x
def Radius(x):
	y = Normalize(Radii[str(x)])
	return float(y)
def Color(x):
	y = Colors[x]
	return y
def distance(A,B):
	D = ((B[0]-A[0])**2 + (B[1]-A[1])**2 + (B[2]-A[2])**2)**(1/2)
	return D
def Normalize(x):
	x = (float(x)/0.77)*0.20
	return x

X = X_Shift(x)

O = (X,0,0)
A1 = (X, 0, distance(A2,A3))
A2 = (X, 1, 0.5)
A3 = (X, 1, -0.5)
A4 = (X, 0,-1*distance(A2,A3))
A5 = (X, -1, -0.5)
A6 = (X, -1, 0.5)

BC = ['white','black','blue','red','grey','red','pink','cyan']

Radii = {'O': 0.73, 'N': 0.75, 'C': 0.77, 'He': 0.32, 'H': 0.37, 'S': 1.02, 'Cl': 0.99, 
    'F': 0.71, 'Xe': 1.30, 'Si': 1.11, 'B': 0.82, 'P': 1.06, 'Br': 1.14}

Valency = {'O': 6, 'N': 5, 'C': 4, 'He': 0, 'H': 1, 'S': 6, 'Cl': 7, 
    'F': 7, 'Xe': 8, 'Si': 4, 'B': 3, 'P': 5, 'Br': 7}

Colors = {'O': 'red', 'N': 'green', 'C': 'black', 'He': 'cyan', 'H': 'white', 'S': 'yellow',
	'Si': 'purple', 'Xe': 'pink', 'F': 'blue', 'Cl': 'green', 'B': 'pink', 'P': 'orange',
	'Br': 'red'}

Sigma_bonds = { A1 : [A6, A2], A2 : [A1, A3], A3 : [A2, A4], A4 : [A3, A5], A5 : [A4, A6], A6 : [A5, A1] }

Pi_bonds = { }

Benzene = Sphere(.000001, color='white').translate(O)

def atomic_class(G,X):

	A = str(input('Enter press enter for simple Benzene model, otherwise type "1": '))
	if (A == '1'):	
		B = str(input('Enter first non carbon ring constituent, type STOP to stop: '))
		C = str(input('Enter non carbon position: '))
		#while (A != ""):
			#if (A == 'SIMPLE MODEL, INSERT FUNCTION'):

		while (B != 'STOP'):
			Coordinate = Coords(0,C)
			Rad = Radius(B)
			Col = Color(B)
			Occupancy.append(Coordinate)
			G += Placement(G,Rad,Col,Coordinate)
			if (B == 'STOP'):
				break
			B = str(input('Enter additional non carbon ring constituent, type STOP: '))
			C = str(input('Enter additional non carbon position or hit enter to launch graphic: '))

	G += Carbon_placement(G, Occupancy, 0)
	return G
		

	for i in Coords[:]:
		if i in L:
			Coords.remove(i)
		if i not in L:
			x += Sphere(0.2, color = 'black').translate(i)
	Pi_Opt = str(input('Are there any pi bonds present? Yes or No: '))
	if Pi_Opt == 'No':
	elif Pi_Opt == 'Yes':
		Bonds = str(input('Enter pi bond atomic pair in format "A1-A2" or type PLOT: '))
		
		while (Bonds != 'PLOT'):
			Bonds2 = Bonds.split('-')
			for bond in Bonds2:

			for atom in Coords:
				for pi_atom in pi_list:
				#if atom in pi_Check?
					if (atom != pi_list[0]) and (atom != pi_list[1]) and (float(distance(pi_atom,atom))==float(distance(A1,A2))):
						x += single_bond(pi_atom, atom)

			if (Bonds == 'PLOT'):
				break
			Bonds = str(input('Enter additional pi bonds or type PLOT: '))
	else:
		print('Not sure what you were trying to do. Exiting now.')
		sys.exit()

	x += Hydrogen(x,Coords,X)
	return x

> list1 = [1,2,3,4]
> list2 = [1,2,3,4]
> list3 = ['a','b','c','d']
> dict(zip(zip(list1,list2),list3))
{(3, 3): 'c', (4, 4): 'd', (1, 1): 'a', (2, 2): 'b'}

Substituent_Dict = dict(zip(Position_labels, H_list))

l = [1,2,3,4,5]

def add(a,b,c,d,e):
	print(a,b,c,d,e)

	add(*l)

1 2 3 4 5

def add(a, b, c):
print(a, b, c)
x = (1, 2, 3)
add(*x)
1 2 3
x = { 'a': 3, 'b': 1, 'c': 2 }
add(**x) 
3 1 2

For unpacking a whole dictionary!

ie {A1: [A1-A2], A2: [A2-A3], A3: [A3-A4]}

def bonder(a,b,c,d,e,f):
------------------------------

sage: 'hello'[::-1]
output: 'olleh'

sage: 'hello'[::-1]
'olleh'



sage: seq_string = 'Python'

sage: print(list(reversed(seq_string)))

['n', 'o', 'h', 't', 'y', 'P']

sage: A = [1,2,3,4,5,6,7]

sage: print(list(reversed(A)))

[7, 6, 5, 4, 3, 2, 1]

set takes individual values then converts them into a set 
 
 print(set(('a', 'e', 'i', 'o', 'u')))
{'o', 'i', 'a', 'u', 'e'}

Can take dictionary keys and turn them into a set 
print(set({'a':1, 'e': 2, 'i':3, 'o':4, 'u':5}))

{'o', 'i', 'a', 'e', 'u'}

number = [3, 2, 8, 5, 10, 6]

largest_number = max(number);

print("The largest number is:", largest_number)

sage: A = [1,2,3,4,5,6,7]

sage: x

[7, 6, 5, 4, 3, 2, 1]

sage: x = A[::-2]

sage: x

[7, 5, 3, 1]

sage: x = A[::2]

[1, 3, 5, 7]

from directory_name.file_name import function_name
-------------------------------------------------------------------------------------------------------------------------
def sphere_and_plane(x):
	return sphere((0,0,0),1,color='red',opacity=.5)+parametric_plot3d([t,x,s],(s,-1,1),(t,-1,1),color='green',opacity=.7)
	sp = animate([sphere_and_plane(x) for x in sxrange(-1,1,.3)])

def frame(t):
	return implicit_plot3d((x^2 + y^2 + z^2), (x, -2, 2), (y, -2, 2), (z, -2, 2), plot_points=60, contour=[1,3,5], region=lambda x,y,z: x<=t or y>=t or z<=t)
	a = animate([frame(t) for t in srange(.01,1.5,.2)])
	a[0]       # long time

	a.show()   # optional -- ImageMagick

-------------------------------------------------------------------

A = { 1 : (0, -1, 0), 2 : (0, 1, 0) }

Central_Atoms = { A[1] : 'O', A[2] : 'O' }

Atom_Sort = { } 

Atoms = Central_Atoms

Atoms

{(0, -1, 0): 'O', (0, 1, 0): 'O'}

Atom_Sort = defaultdict(set)

for k, v in Atoms.items():

	Atom_Sort[v].add(k)

Atom_Sort

#output: defaultdict(<class 'set'>, {'O': {(0, 1, 0), (0, -1, 0)}})

set.union(*Atom_Sort.values())

#output: {(0, -1, 0), (0, 1, 0)}






